//
//  ViewController.m
//  UIImage
//
//  Created by admin on 06/12/15.
//  Copyright (c) 2015 admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
-(IBAction)buttonTapped:(UIButton *)sender{
     NSLog(@"Button Tapped!");
   if ([usernamefield.text isEqualToString:@"saketh" ]||[passwordfield.text isEqualToString:@"reddy"]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"The Username is:" message:@"Correct" delegate:self cancelButtonTitle:@"dismiss" otherButtonTitles:nil];
            [alert show];
    }
    else{
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"The Username is:" message:@"Incorrect" delegate:self cancelButtonTitle:@"dismiss" otherButtonTitles:nil];
        [alert1 show];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"saketh",@"usernamefield",@"reddy",@"password", nil];

    UILabel *bgColor = [[UILabel alloc]init];
    bgColor.backgroundColor=[UIColor colorWithRed:203/255.0f green:203/255.0f blue:203/255.0f alpha:1.0];
    bgColor.frame=self.view.frame;
    [self.view addSubview:bgColor];
    
//    UIImage *pic = [UIImage imageNamed:@"Aerial08.jpg"];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:pic];
//    [self.view addSubview:imageView];
    
  

    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(200, 350, 200, 200)];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonTapped:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle : @"Login" forState:UIControlStateNormal];
    button.layer.cornerRadius = 5;
    button.backgroundColor = [UIColor colorWithRed:33/255.0f green:112/255.0f blue:141/255.0f alpha:1.0f];
    button.frame=CGRectMake(190, 370, 170, 50);
    [self.view addSubview:button];
    
    usernamefield = [[UITextField alloc]init];
    usernamefield.textColor = [UIColor blackColor];
    usernamefield.backgroundColor= [UIColor lightTextColor];
    usernamefield.frame=CGRectMake(75, 120, 200, 50);
    usernamefield.borderStyle=UITextBorderStyleRoundedRect;
    usernamefield.placeholder= @"Username";
    [self.view addSubview:usernamefield];
    
    passwordfield = [[UITextField alloc]init];
    passwordfield.textColor = [UIColor blackColor];
    passwordfield.secureTextEntry = YES;
    passwordfield.backgroundColor= [UIColor lightTextColor];
    passwordfield.frame=CGRectMake(75, 200, 200, 50);
    passwordfield.borderStyle=UITextBorderStyleRoundedRect;
    passwordfield.placeholder= @"Password";
    [self.view addSubview:passwordfield];
    

    UILabel *app = [[UILabel alloc]initWithFrame:CGRectMake(100,0,200,175)];
    app.text = @"MY APPLICATION";
    app.textColor = [UIColor blackColor];
    [self.view addSubview:app];
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
